'''
Created on May 23, 2012

@author: mroch
'''

# updates the itis collection from source document archive

import argparse
import requests

# Tethys specific modules
import client_server

import pdb

# Note:  When updating all collections, order can be important.
# The order below satisifies potential dependencies.
all_collections = [
    'ITIS_ranks',
    'ITIS',
    'SourceMaps',
    'SpeciesAbbreviations',  # Depends on ITIS_ranks, ITIS
    # These may depend on SourceMaps
    'Deployments',
    'Calibrations',
    # These may depend on ITIS_ranks, ITIS, SourceMaps, SpeciesAbbreviations
    'Detections',
    'Localizations',
]

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="""
Add XML documents from source materials stored on the server.
This is useful for rebuilding a database after a catastrophic failure
or widespread changes to the source material.

Specifying "all" will add the following collections:\n%s
"""%(", ".join(all_collections)))
    
    client_server.add_client_opts(parser)

    parser.add_argument('--update', type=client_server.true_false, default=False, 
                      help="""If a document exists, should it be updated?
                      Default false.""" )
    parser.add_argument('--clear', type=client_server.true_false, default=False, 
                      help=
                      "Clear container before updating documents.  This "+
                      "will not remove any source documents that are "+
                      "archived but any sources from databases will "+
                      "have to be imported again.  Think twice before "+
                      "using.  Default false.")
    parser.add_argument('collections', nargs='+',
                        help='Replace collections with space separated list of '+
                        'containers to be updated. ' +
                        'Use "all" to update all containers.  Note that ' +
                        'there may be dependencies that could cause ' +
                        'failures and order is important.  Recommended ' +
                        'order:  %s'%(", ".join(all_collections)))
    options = parser.parse_args()

    collections = options.collections
    if not options.collections:
        parser.error("At least one collection must be specified")
    elif "all" in options.collections:
        collections = all_collections

    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
        
    print("Connecting to Tethys server:  %s %s" % (server_url, description))
    

    boolTo01 = {True:"1", False:"0"}
    msgparams = {}
    for item in ["update", "clear"]:
        msgparams[item] = boolTo01[getattr(options, item)]

    for c in collections:
        url = server_url + "/%s/rebuild"%(c)
        print("Updating %s:  %s"%(c, url))
        result = requests.post(url, params=msgparams)
        if result.status_code != requests.codes.ok:
            print('%s error occurred: '%(c), result.status_code)
        print(result.text)

                

main()
