'''
Created on May 23, 2012

@author: mroch
'''


from base64 import b64encode
import getpass
import mimetypes
import argparse
import os
import xmlrpc.client
import sys

import xml.dom.minidom
from xml.dom.minidom import Node

# Python extension modules
import requests
import xpath

# Tethys specific modules
import client_server
import mime

import pdb

errexit = 5

requestb = "http://requestb.in/sxodgbsx"
requestDebug = False

usage = """
Tethys data import
Add a datasource to a collection.

Datasources are local to the client (--file) or accessible from the server, 
depending upon the type of data.  

Local data sources:
    MicrosoftExcel or other Excel compatible workbook
    csv (comma seperated value list)
    XML (Extended markup language document) - XML documents are assumed
      to conform to the database schema and no translation will be done.

    
If the collection is one of the standard Tethys collections and
the datasource is local, a copy of the source document will be
archived on the server.  

Standard collections:  Detections, Deployments, Ensembles, 
  itis, Localizations, and SourceMaps. 
"""
def main():
    parser = argparse.ArgumentParser( formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
    
    client_server.add_client_opts(parser)
    parser.add_argument('collection',metavar='Collection',help='Destination container for the submission.')
    parser.add_argument("--file", action='store', default=None,
                      help="".join([
                      "file can be used for any file that is specified on the ",
                      "command line such as a local Excel, CSV, XML, or Access", 
                      "document."]))
    parser.add_argument("--ConnectionString", action='store', default=None, 
                      help="".join([
                      "Open database connection string.  Can be used for ",
                      "connecting to any data source with an ODBC driver.  "
                      "See manual for details as well as ",
                      "http://www.connectionstrings.com for information.  To ",
                      "prompt for a password, use password=<*Password*> in the connection",
                      "string."]))
    parser.add_argument("--speciesabbreviations", action='store', default='',
                      help="Name of species abbreviation map")
    parser.add_argument("--sourcemap", action='store', default='', 
                      help="".join([
                      "Map name from SourceMaps collection.  May be omitted ",
                      "if it is not needed (e.g. XML).  Sourcemaps either use ",
                      "<Table> or <Sheet> directives.  <Sheet> directives are ",
                      "used whenever there is no --ODBC option and assume ",
                      "that the data consists of row oriented data.  In ",
                      "contrast, <Table> permits richer queries to data ",
                      "sources that can be treated as relational databases."]))
    parser.add_argument("--overwrite", type=client_server.true_false, default=False,
                      help="Overwrite an existing resource?")
        
    options = parser.parse_args()
    
    if not options.collection:
        parser.ArgumentError("Collection was not specified")
    else:
        collection = options.collection

    # Establish connection to server
    (options.url, options.urlDescription) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)

    print("<!-- server:  %s %s -->" % (options.url, options.urlDescription))

    if options.file != None:
       # file to mimetype
       if "," in options.file:
           parser.error("Filename cannot contain a comma")
       (mimetype, encoding) = mime.getMimeType(options.file)
       
       rest_send(collection, options, mimetype, encoding)
    else:
        if options.ConnectionString == None:
            parser.error("".join(["When --file is not specified, ",
            "--connectionstring must specify a networked (or server ",
            "accessible resource"]))
        else:
            rest_send(collection, options, None, None)
               
def get_password(str):
    """password - Prompt for password if place holder is present in str.
        Returns str with user password"""
        
    promptpw = "Password=<*Password*>;"
    if promptpw in str:
        password = getpass.getpass("Password:  ")
        str = str.replace(promptpw, "Password=%s;"%(password))
    return str

def rest_send(collection, options, mimetype, encoding):
    
    specification_xml ='<?xml version="1.0" encoding="UTF-8"?><import><docname/><overwrite>%s</overwrite><species_map>%s</species_map><source_map>%s</source_map><sources><source><type>file</type><file>%s</file></source></sources></import>'
    
    
    
    
    debug = False

    url = options.url

    # Convert booleans appropriately
    boolmap = {False: '0', True: '1'}

    params = {}
    if options.speciesabbreviations:
        params['species_abbr'] = options.speciesabbreviations            
    if options.sourcemap: 
        params['import_map'] = options.sourcemap
    params['overwrite'] = boolmap[options.overwrite]
    url = url + "/%s"%(collection)
    if options.file:
        
        #format the specification xml
        specification_xml = specification_xml%(options.overwrite,options.speciesabbreviations,options.sourcemap, os.path.basename(options.file))
        params['specification'] = specification_xml
        
        
        # User is sending some type of file (spreadsheet, mdb, etc.)
        url = url + "/import"

        try:
            f = open(options.file, 'rb')
        except Exception as e:
            raise ValueError("Unable to open file %s"%(options.file))
    
        fbasename = os.path.basename(options.file)
        [fdocId, _] = os.path.splitext(fbasename)
        fpath = os.path.dirname(options.file)
        
        files = {fbasename : (fbasename, open(options.file, 'rb'), mimetype)}

    else:
        url = url + "/ODBC"
        f = None
        files = None

    if options.ConnectionString != None:
        params['ConnectionString'] = get_password(options.ConnectionString)
    r = requests.post(url, params=params, files=files)
    
    if r.status_code == requests.codes.bad:
        dom = None
        try:
            dom = xml.dom.minidom.parseString(r.text)
        except Exception as e:
            # Unable to parse the result, just print it
            print(r.text)
            print("--- Unable to parse server response, exiting... ---")
            print(e)
            sys.exit(errexit)
        else:
            xqry = "//Import/Document/*[not(self::Media/Missing) and not(self::Media/Warnings)]"
            otherprobs = xpath.findvalue(xqry, dom)
            if otherprobs != None:
                print(r.text)
                sys.exit(errexit)
            else:
                missing = get_attachment_list(files, dom)
                attach(files, missing, fdocId, fpath)
                # XML data document was already sent which means that the
                # open file was read.  Rewind to beginning.
                files["data"][1].seek(0)
                r = requests.post(url, params=params, files=files)
                print(r.text)
                if r.status_code == requests.codes.bad:
                    sys.exit(errexit)
    else:
        print(r.text)

    
def get_attachment_list(files, dom):
    
    missing = {}
    mediatypes = xpath.find('//Import/Document/Media/Missing/*', dom)
    # Elements underneath Missing indicate the media type (e.g. Audio)
    # and a list of File elements.  Pull out the File elements for each type
    # and save them.
    for n in mediatypes:  # for each type of missing media
        mediatype= n.nodeName
        if mediatype not in missing:
            missing[mediatype] = []
        attachments = xpath.findvalues('File', n)
        missing[mediatype].extend(attachments)
        
    return missing

def attach(files, filelist, docid, basedir):
    """attach(files, filelist, basename, basedir) - Add the specified files
    files - Dictionary of data to be passed to server, new files are
        added as AttachmentN.
    filelist - dictionary of different types of files.  Each type has a list
        of filenames.
    docid - document identifier for XML document that these files are to be 
        associated with
    basedir - Location of document docid in the file system.  Attachments
        are expected to be in one of the following directories:
        [basedir-type, basedir-attach/type] where type is a filelist dictionary key.
    """
    
    count = 1
    missing = []
    for atype in list(filelist.keys()):
        # Build list of directories where attachments might be relative
        # to the basedir
        dirs = list(filter(os.path.exists,
                      [os.path.join(basedir,  docid + '-' + atype),
                       os.path.join(basedir, docid+'-attach', atype)]))
        for fname in filelist[atype]:
            # Find file
            found = False
            idx = 0
            pathtofname = None
            while not found and idx < len(dirs):
                pathtofname = os.path.join(dirs[idx], fname)
                if os.path.exists(pathtofname):
                    found = True
                else:
                    idx = idx + 1
            if not found:
                missing.append(fname)
            else:
                # attach the file
                (mimetype, encoding) = mime.getMimeType(pathtofname)
                files['Attachment%d'%(count)] = \
                  (os.path.basename(pathtofname), 
                   open(pathtofname, 'rb'), mimetype)
                count = count + 1

def rest_post(url, params):
    "POST to REST server"
    result = request.post(url, params)
    
    if result.status_code != requests.codes.ok:
        print('error ocurred: %d'%(result.status_code))
        print(result.text)
    else:
        dom = xml.dom.minidom.parseString(results.text)
        print(result.text)
        
    return result    

    
main()
