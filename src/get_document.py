'''
Created 06/09/14

Retrieves a specific document in XML format

@author: Sherbert/Mroch
'''
import argparse
import os
import sys

# Tethys 3rd party module
import requests

# Tethys specific modules
import client_server

import pdb


UNSUPPORTED = 10

usage = """
Retrieve a specific document in XML format
"""

def main():
    parser = argparse.ArgumentParser(description=usage)
    
    client_server.add_client_opts(parser)
    parser.add_argument('collection',metavar='Collection',
                        help='Collection containing the target document.')
    parser.add_argument("--docid", type=str, default=None,
                      help="".join([
                      "name of the document of interest"]))
    parser.add_argument("--outpath", type=str, default=None,
                      help="".join([
                      "The path to save the retrieved XML document"]))
    
    options = parser.parse_args()
    collection = options.collection
    
    if not collection:
        print("Error: Must specify which collection to retrieve document from")
    if not options.docid:
        print("Error: Missing Document ID, exiting")
        sys.exit()
    if not options.outpath:
        print("Error: Missing output path, exiting")
        sys.exit()
        
    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
    
    print("Connecting to server:  %s %s..." % (server_url, description))
    if options.servertype == "REST":
        url = server_url +"/%s/?DocId=%s" % (collection,options.docid)
        result = requests.get(url)
        
        if result.status_code != requests.codes.ok:
            print("Error code %d\n%s\n"%(result.status_code, result.text))
            sys.exit(result.status_code)
        else:
            print("Document Retrieved. Saving...")
            outfile = os.path.join(options.outpath,options.docid+'.xml')
            try:
                f = open(outfile,'w')
                f.write(result.text)
                f.close()
                print("Complete; view at "+outfile)
            except IOError as e:
                print("I/O error({0}): {1}".format(e.errno, e.strerror))

    else:
        print("Unsupported server type:  %s"%(options.servertype))
        sys.exit(UNSUPPORTED)
            
