import argparse
import os
import sys

# Tethys 3rd party module
import requests

# Tethys specific modules
import client_server

import pdb


UNSUPPORTED = 10

usage = """
Instruct server to log benchmarks
"""
def benchmark(options):
    """

    :param options: Object with attributes
       server - Tethys server name
       port - Port over which Tethys is served
       secure_socket_layer - Use SSL?
       enabled - Set benchmarking on (true) or off (false)
    :return: None
    """

    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
    

    print(f"Connecting to server:  {server_url} {description}...")

    url = server_url +"/Tethys/performance_monitor/" + options.enabled
    result = requests.put(url)

    if result.status_code != requests.codes.ok:
        print(f"Error code {result.status_code}" 
              "\n%s\n"%(result.text))
        sys.exit(result.status_code)
    else:
        print(result.text)

            
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=usage)

    client_server.add_client_opts(parser)
    parser.add_argument('enabled', metavar='Enable', type=str,
                        help='Turn on/off performance monitor (true|false)')

    options = parser.parse_args()

    benchmark(options)