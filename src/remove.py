'''
Created on May 23, 2012

@author: mroch
'''


from base64 import b64encode
import getpass
import argparse
import os
import xmlrpc.client
import requests

# Tethys specific modules
import client_server

import pdb

usage = """
Tethys remove a document by name
"""
def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description = usage)
    parser.add_argument('collection',metavar='Collection',
                        help='Collection containing the target document.')
    parser.add_argument("docid", metavar='DocumentID',
                      help="".join([
                      "name of the document of interest"]))
    
    client_server.add_client_opts(parser)
    
    options = parser.parse_args()
    
    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
    
    print("Connecting to server:  %s %s..." % (server_url, description))
    
    collection = options.collection
    docname = options.docid
    url = server_url +"//%s/?DocId=%s"%(collection,docname)
    
    result = requests.delete(url)
    if result.status_code != requests.codes.ok:
        print('%s error ocurred:\n'%(result.status_code))
        print(result.text)
    else:
        print(result.text)
            
    
main()
