import mimetypes
import os

mime = mimetypes.MimeTypes()
# add missing types
mime.add_type('application/msaccess', '.mdb')
mime.add_type('text/csv', '.csv')
mime.add_type('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                   '.xlsx', True)
mime.add_type('application/vnd.ms-excel', '.xls', True)

def getMimeType(fname, mimetype=None):
    """getMimeType - Extract mimetype from a filename or extension if not specified
    Returns mimetype if specified, otherwise guesses from extension that
    must contain the period (.ext)
    """
    
    if mimetype == None:
        mimetype = mime.guess_type(fname)
    return mimetype
