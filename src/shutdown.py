
import argparse
import os
import sys

# Tethys 3rd party module
import requests

# Tethys specific modules
import client_server

import pdb


UNSUPPORTED = 10

usage = """
Instruct server to begin shutdown
"""
def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description = usage)
    
    client_server.add_client_opts(parser)
    
    options= parser.parse_args()
    
    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
    
    print("Connecting to server:  %s %s..." % (server_url, description))

    url = server_url +"/Tethys/shutdown"      
    result = requests.put(url)
    
    if result.status_code != requests.codes.ok:
        print("Error code %d\n%s\n"%(result.status_code, result.text))
        sys.exit(result.status_code)
    else:
        print(result.text)

            
    
main()