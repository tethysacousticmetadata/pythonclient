import argparse
import requests
import sys
import os
parent_dir = os.path.dirname(
    os.path.dirname(
        os.path.realpath(__file__)
    )
)

sys.path.append(parent_dir)  # look in parent as well

# Tethys specific modules

import client_server

# Collections within the namespace
all_collections = [
    "Calibrations",
    "Deployments",
    "Detections",
    "Ensembles",
    "Events",
    "Localizations",
]

import pdb

def update(options):
    if len(options.collections) == 1 and 'all' in options.collections:
        options.collections = all_collections

    # Generate server URL
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)

    print("Updating namespace for collection:")
    for c in options.collections:
        print(c)
        result = requests.put(f'{server_url}/Upgrade/namespace/{c}')
        if result.status_code != requests.status_codes.codes.ok:
            print(f"Error processing collection {c}")
            print(result.text)
    print("All done, if no errors occurred, you should have seen"
          " a list of collection names with no additional output")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""
    Modify all documents in specified collections such that all
    elements are within namespace http://tethys.sdsu.edu/1.0

    Specifying "all" will add the following collections:\n%s
    """ % (", ".join(all_collections)))

    client_server.add_client_opts(parser)
    parser.add_argument('collections', nargs='+',
                        help=('Replace collections with space separated '
                             'list of containers to be updated.  Use ' +
                             '"all" (default) to update all containers.'),
                        default='all'
                        )

    options = parser.parse_args()

    update(options)
