from client import Query


report_deploymentId = """
declare namespace ty = "http://tethys.sdsu.edu/schema/1.0";

<ty:deployments> {
  for $d in collection("Deployments")/ty:Deployment
  return
    <deployment> 
      <uri>{base-uri($d)}</uri>
      {$d/Id} 
    </deployment>
}
</ty:deployments>
"""

# This query is designed to be idempotent
# If you run it twice, will not add a second ID (nor will it change it,
# which might be desirable)
swfsc_add_deploymentId = """
import module namespace functx="http://www.functx.com" at "functx.xq";
declare namespace ty = "http://tethys.sdsu.edu/schema/1.0";

for $d in collection("Deployments")/ty:Deployment
let $id := concat(
   $d/Project/text(),
   functx:pad-integer-to-length(xs:integer($d/DeploymentID), 3),
   "-", $d/Site/text()
   )
return
  if ($d/Id)
     then 
       ()
     else
       insert nodes
           <Id>{$id}</Id>
           as first into $d          
"""

add_detectionsId = """
import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";

for $Detections in collection('Detections')/ty:Detections
   (: Use base-uri without the dbxml://collection name :)
   let $newid := replace(base-uri($Detections), "dbxml:///Detections/", "")
   return
     if ($Detections/Id)
     then
       ()
     else
       insert nodes
           <Id>{$newid}</Id>
           as first into $Detections
"""

verify_has_deployments_old = """
import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";

<ty:Result>
{ 
  for $Detections in collection('Detections')/ty:Detections
  let $deployment := collection('Deployments')/ty:Deployment[
        Project = $Detections/DataSource/Project and
        DeploymentID = $Detections/DataSource/Deployment and
        Site = $Detections/DataSource/Site]
  return
     if ( $deployment )
      then
        <matched> {base-uri($Detections)} </matched>
      else 
        <BAD> 
           <uri> {base-uri($Detections)} </uri>
          {$Detections/DataSource}
       </BAD>
}
</ty:Result>
"""

verify_has_deployments_new = """
import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";

<ty:Result>
{ 
  for $Detections in collection('Detections')/ty:Detections
  let $deployment := collection('Deployments')/ty:Deployment[
        Id = $Detections/DataSource/DeploymentId]
  return
     if ( $deployment )
      then
        <matched> {base-uri($Detections)} </matched>
      else 
        <BAD> 
           <uri> {base-uri($Detections)} </uri>
          {$Detections/DataSource}
       </BAD>
}
</ty:Result>
"""

update_detections_datasource = """
declare default element namespace "http://tethys.sdsu.edu/schema/1.0";

for $Detections in collection('Detections')/Detections
  let $Deployment := collection('Deployments')/Deployment[
        Project = $Detections/DataSource/Project and
        DeploymentId = $Detections/DataSource/Deployment and
        Site = $Detections/DataSource/Site]
  return
     if ( $Deployment )
      then (: has old style reference :)
			replace node $Detections/DataSource with
			  <DataSource><DeploymentId>{$Deployment/Id/text()}</DeploymentId></DataSource>
      else 
        ()

"""

bak = """
  if ($det/DataSource/Project) 
     then  (: has old style DataSource :)
       replace node $det/DataSource with
           <DataSource>
              <DeploymentId>{$dep/Id}</DeploymentId>
           </DataSource>
	 else ()

"""

def main():
    query = Query()  # use defaults

    if True:
        # print("Adding Deployment/Id")
        # query.xquery(swfsc_add_deploymentId)
        #query.xquery(report_deploymentId, indent=True, verbose=True)

        print("Adding Detections/Id")
        query.xquery(add_detectionsId)

    if False:
        query.xquery(verify_has_deployments_old, indent=True, verbose=True)

    if False:
        print("Modifying Detections references to Deployments")
        query.xquery(update_detections_datasource, indent=True, verbose=True)
        query.xquery(verify_has_deployments_new, indent=True, verbose=True)

    # todo:  reindex containers

    print("complete")

if __name__ == '__main__':

    print("Do not execute this script if you do not know what you are doing.")
    print("It modififies Tethys database structures to conform with Tethys >= 3.0")
    print("It assumes that your database containers have already been migrated ")
    print("to the new format via the tethys2to3.bat migration batch tool.")
    print()
    main()
