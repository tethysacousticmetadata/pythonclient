# Sample Python client
# Shows example of running an XQuery

import math
import re
import argparse
import xml.etree.ElementTree as ET

# site packages
import requests  # http library

# our modules
import client_server

class Query:

    def __init__(self, server="http://localhost", port=9779):
        """

        """
        # Logic to handle server coming in with a port number
        if len(server.split(":")) > 2:
            # User put in a port
            self.url = server
        else:
            self.url = f"{server}:{port}"
        if not self.url.startswith("http"):
            server.url = "http://" + server.url

    def xquery(self, query, indent=False, verbose=False):

        if verbose:
            for n, l in enumerate(query.split("\n")):
                print(f"{n:3d} {l}")

        params = {"XQuery": query}  # message parameters
        result = requests.post(self.url + "/XQuery", params)

        if result.status_code != requests.codes.ok:
            if result.status_code == requests.codes.bad_request:
                raise ValueError(f"BAD_REQUEST:  Server response:\n{result.text}")
            else:
                result.raise_for_status()

        xml = result.text

        if indent and len(xml) > 0:
            # provide proper indentation
            try:
                element = ET.XML(xml)
                ET.indent(element)
                xml = ET.tostring(element, encoding='unicode')
            except ET.ParseError as e:
                print("Unable to indent result due to parsing error")


        if verbose:
            print("Server response")
            print(xml)

        return xml

    def xquery_from_file(self, filename, verbose=False):
        """
        Run XQuery contained in file
        :param filename: file containing XQuery
        :param verbose: debug information if True
        :return:
        """

        # Read the file
        with open(file, "rt") as in_h:
            query = in_h.readlines()


        return self.xquery(query, verbsose)


    
    
    

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""%prog - XML Database Server
        Default values for choices are marked by an *""")

    client_server.add_client_opts(parser)
    options = parser.parse_args()
    
    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
    
   
       # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
        
    print("Connecting to %s server:  %s %s" % (options.servertype, 
                                               server_url, description))
    
    if options.servertype == "REST":
        server = server_url
        xquery = xquery_REST # set appropriate function
    else:
        raise ValueError("Unsupported servertype protocol")

    # Sample query, show all Zipheus cavirostris detections from 
    # site M deployment 38 in project SOCAL
    print("Running query")
    result = xquery(server, '''
(: import Tethys schema so we have type information about the docuements :)
import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
(: import utility functions :)
import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";


let $xmldocument := 0  (: first document :)
let $xmldocs := ()     (: no documents so far :)

(: Find all of the desired detections.  
   This query is a bit more complicated than it need be as we are tracking
   which XML document the detections came from.  
   
   This is sometimes useful, especially when there are multiple lines
   of effort across the same time and space, although it is not really 
   needed here.
   
   We create a temporary variable with all of the detections and then sort them
   in the next query.  Again, this is overengineering for this specific query,
   but would be needed to return detections from multiple deployments in order.
:)
let $detections := for $item in (
    for $det at $p in collection("Detections")/ty:Detections
        let $xmldocument := $xmldocument + 1
        let $xmldocs := ($xmldocs, $det/DataSource)
        (: conditions on Detections group :)
        where upper-case($det/DataSource/Site) = upper-case("M") and 
              upper-case($det/DataSource/Project) = upper-case("SOCAL") and
              $det/DataSource/Deployment = 38.000000 
        return
           for $detection in $det/OnEffort/Detection  (: OnEffort/OffEffort :)
              (: conditions on individual Detections, here we use
                 an abbreviation for the species name  :)
              where $detection/SpeciesID = lib:abbrev2tsn("Zc", "NOAA.NMFS.v1") 
           return
                <Detection>
                  {$detection/Start}
                  {$detection/End}
                  <idx>{xs:integer($p)}</idx>
                </Detection>
        )
  return $item

(: Sources contains the list of indices corresponding to all of the documents
   from which we drew detections :)
let $sources := distinct-values(for $d in $detections return $d/idx)

(: Finally, we return the detections and a list of the data sources
   from which they came. :)
   
return
<ty:Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<Detections>
  {(: Rewrite the detections mapping each detection to a deployment/ensemble :)
   for $d in $detections
   order by $d/Start
   return
     <Detection>
      {$d/Start}
      {$d/End}
      <idx>{index-of(($sources), $d/idx)}</idx>
     </Detection>
  }
</Detections>
<Sources>
  {
   (: Write out the information for the deployment/ensembles :)
   for $source in $sources
     return
      collection("Detections")[xs:decimal($source)]/ty:Detections/DataSource
  }
</Sources>
</ty:Result>
''')

    print(result)    


if __name__ == "__main__":
    main()
