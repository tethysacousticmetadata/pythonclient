'''
Created on October 13, 2013

@author: mroch
'''

# updates the itis collection from source document archive

import argparse
import requests

# Tethys specific modules
import client_server

usage ="""
Create a database checkpoint.  After successful completion, all
transactions are guaranteed to be written to the database, and
database log files may be removed with the exception of the 
current log file."""

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description = usage)
    
    client_server.add_client_opts(parser)
    
    options = parser.parse_args()
        
    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
        
    print("Connecting to server:  %s %s" %(server_url, description))

    msgparams = {}
    url = server_url + "/Tethys/checkpoint"
    print("Checkpointing:  %s"%(url))
    result = requests.put(url, params=msgparams)
    print(result.text)

main()
