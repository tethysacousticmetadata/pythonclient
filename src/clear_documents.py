'''
Created on May 23, 2012

@author: mroch
'''

# updates the itis collection from source document archive

import argparse

# site modules
import requests

# Tethys specific modules
import client_server

import pdb

usage = """
    Clear documents in the specified collection(s).
    All source documents related to the container will be moved to the DeletedArchive folder.
    Any files sharing names in the DeletedArchive will be overwritten.
    In other words, only the most recently deleted files are archived.
    USE WITH CAUTION"""

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description = usage)
    parser.add_argument('collection',metavar='Collection',nargs='+',
                        help='Destination container(s) for the operation.')
    
    client_server.add_client_opts(parser)
    options = parser.parse_args()
    
    collections = options.collection
    if not collections:
        print("Please specify a document container.")
        return
        
    # Establish connection to server
    (server_url, description) = client_server.get_url(
        options.server, options.port, options.secure_socket_layer)
        
    print("Connecting to server:  %s %s" % (server_url, description))

    msgparams = {}
    msgparams['DocId'] = "<*clear*>"  # delete all documents
    for c in collections:
        # DELETE all items in each resource.
        result = requests.delete(server_url + "/%s"%(c), params = msgparams)
        if result.status_code != requests.codes.ok:
            print('error occurred: ', result.status_code)
        else:
            print(c)
            print(result.text)
            
main()
